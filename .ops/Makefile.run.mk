docker_run:
	docker run -d --name=mosquitto_test_run --net host homesmarthome/mosquitto:latest
	docker run -d \
	  --name=chromecast_test_run \
	  --net host \
	  -v $(PWD)/.env:/env \
	  -v $(PWD)/.test/config.ini:/chromecast/config.ini \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep phoscon_test_run

docker_stop:
	docker rm -f chromecast_test_run 2> /dev/null; true
	docker rm -f mosquitto_test_run 2> /dev/null; true